# GitLab CI/CD Pipeline for Maven Java Application to AWS ECR

This project demonstrates a GitLab CI/CD pipeline for building and pushing a Maven Java application to Amazon Elastic Container Registry (ECR). The pipeline uses Docker images and AWS CLI for authentication.

## Project Structure

- **Stages:**
    - **build-push:** Build the Docker image and push it to AWS ECR.
    
- **Variables:**
    - **AWS_ECR_REGISTRY_URI:** The URI of your AWS ECR repository (e.g., **098150418900.dkr.ecr.us-east-1.amazonaws.com/maven-job**).
    - **ECR_REPOSITORY_NAME:** The name of your ECR repository (e.g., **maven-job**).
    - **DOCKERFILE_PATH:** The path to your Dockerfile (e.g., **./Dockerfile**).

    ## CI/CD Configuration

### **Build and Push Stage (build-push)**
This stage uses the Docker executor with the stable version and Docker-in-Docker service. The pipeline includes the following steps:

1. **Install Dependencies:**

    - Installs Python3, Pip, and AWS CLI in the Docker image.

```yaml
before_script:
  - apk add --no-cache python3 py3-pip && pip3 install --upgrade pip
  - pip3 install --no-cache-dir awscli && rm -rf /var/cache/apk/*
```

2. **Build Docker Image:**

    - Authenticates with AWS ECR.
    - Builds the Docker image using the provided Dockerfile.

```yaml
script:
  - echo "Building Docker image"
  - aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $AWS_ECR_REGISTRY_URI
  - docker build -t $AWS_ECR_REGISTRY_URI:$CI_COMMIT_SHORT_SHA -f $DOCKERFILE_PATH .
  - docker push $AWS_ECR_REGISTRY_URI:$CI_COMMIT_SHORT_SHA
  ```

# **Usage**

1. **AWS Configuration:**

    - Create an ECR repository in AWS and configure the following variables in your GitLab project:
        - **AWS_DEFAULT_REGION:** Your AWS region.
        - **AWS_ACCESS_KEY_ID:** AWS access key with ECR permissions.
        - **AWS_SECRET_ACCESS_KEY:** AWS secret access key.

2. **GitLab Variables:**

    - Configure the following variables in your GitLab project:
        - **AWS_DEFAULT_REGION**
        - **CI_REGISTRY_USER**
        - **CI_REGISTRY_PASSWORD**
3. **Resource-Based Policy in ECR:**

    - Adjusted the resource-based policy in your ECR repository to allow anyone to push Docker images.
Now, every GitLab CI/CD pipeline run will build the Maven Java application, create a Docker image, and push it to the specified AWS ECR repository.

Make sure to replace placeholder values in the variables and adapt the configurations according to your project setup.