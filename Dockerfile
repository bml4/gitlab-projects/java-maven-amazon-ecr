# Use the official maven image as the base image
FROM maven:latest as builder

# Install AWS CLI dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    python3 \
    python3-pip && \
    rm -rf /var/lib/apt/lists/*

# Install AWS CLI using pip3
RUN pip3 install awscli

# Set the working directory to /app
WORKDIR /app

# Copy the pom.xml file to the container

COPY pom.xml .

# Copy the entire source code to the container

COPY src/ /app/src/

# Build the Maven Project
RUN mvn clean package

# use a lightweight base image for the final image
FROM openjdk:11-jre-slim

# Set the working directory to /app
WORKDIR /app

# Copy the JAR file from builder stage
COPY --from=builder /app/target/my-app-1.0-SNAPSHOT.jar ./app.jar

#Expose the port on which your application runs

EXPOSE 8080

# Command  to run the application
CMD ["java", "-jar" "app.jar"]